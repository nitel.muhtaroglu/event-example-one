package cs102;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("CS 102 App");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel mainPanel = new JPanel();
        frame.add(mainPanel);

        JButton button = new JButton("I am a Button");
        mainPanel.add(button);

        button.addActionListener(new MyListener());

        frame.setVisible(true);
    }
}
