package cs102;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyListener implements ActionListener {
    public void actionPerformed(ActionEvent actionEvent) {
        String message = "Button is clicked!";
        JOptionPane.showMessageDialog(null, message);
    }
}
